Ansible SSH Daemon Configuration
================================

Configure the ssh daemon.


Usage
-----

### Role Variables

Available variables are listed below, along with default values (see [`defaults/main.yml`](defaults/main.yml)).

#### `sshd_config`

Key/value list corresponding to sshd configuration variables.

```yaml
sshd_config:
  PasswordAuthentication: "no"
  ChallengeResponseAuthentication: "no"
```

> For the moment, only existing variables in the configuration (possibly in comment) are replaced.

### Example Playbook

```yaml
- hosts: server
  roles:
    
    - role: sebastienm4j.sshd-config
      sshd_config:
        Port: 2222
        PasswordAuthentication: "yes"
```


License
-------

Ansible SSH Daemon Configuration is release under [MIT licence](LICENSE).
